package com.igniteuhc.api.global.backend.backends.file;

import com.igniteuhc.uhc.backend.AbstractBackend;
import com.igniteuhc.uhc.backend.data.DataValueType;

import java.util.UUID;

public class FileBackend extends AbstractBackend{
    public FileBackend() {
        super("filebackend.yml");
    }

    public FileData load(DataValueType type, Object o) {
        return new FileData(String.valueOf(o));
    }
}
