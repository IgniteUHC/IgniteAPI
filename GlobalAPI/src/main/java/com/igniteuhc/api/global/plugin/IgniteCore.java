package com.igniteuhc.api.global.plugin;

import com.igniteuhc.api.global.file.PropertiesFile;
import com.igniteuhc.api.global.ftp.AbstractFileConnection;
import com.igniteuhc.api.global.ftp.FTPFileConnection;
import com.igniteuhc.api.global.ftp.SSHFileConnection;
import com.igniteuhc.api.global.plugin.updater.FileUpdater;
import com.igniteuhc.api.global.plugin.updater.SQLUpdater;
import com.igniteuhc.api.global.plugin.updater.Updatetask;
import com.igniteuhc.api.global.sql.SQLConnection;
import com.igniteuhc.api.global.sql.SQLUtil;
import lombok.Getter;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class IgniteCore<P> implements FileUpdater  {

    private static boolean shutdown = false;

    @Getter
    private static IgniteCore<?> instance;

    private final File sqlpropertiesfile = new File("connect.properties");
    private PropertiesFile propertiesFile;
    private Set<FileUpdater> fileupdaters = new HashSet<>();
    private Set<SQLUpdater> sqlUpdaters = new HashSet<>();

    @Getter
    private SQLConnection connection;

    @Getter
    private AbstractFileConnection fileConnection;

    public IgniteCore() {
        instance = this;
    }

    public final void onEnable() {

        getLogger().log(Level.INFO, "Finding updates table");

        try {

            loadUpdatesTable();

        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, "Could not create updates table", ex);
            endSetup("Could not create updates table");
        }


    }

    public final void onLoad() {

        //Loading properties
        getLogger().log(Level.INFO, "Loading SQL Properties");
        loadProperties();

        //Loading ftp connection
        getLogger().log(Level.INFO, "Initiating File Connection");
        initFileConnection();


    }

    public void addUpdater(FileUpdater updater) {
        fileupdaters.add(updater);
    }

    public void addUpdater(SQLUpdater updater) {
        sqlUpdaters.add(updater);
    }

    public abstract void update(Runnable r);

    public abstract boolean bungee();

    public abstract void runTaskLater(Runnable r, long l, TimeUnit timeUnit);

    public abstract void onIgniteEnable();

    public abstract void onIgniteDisable();

    public abstract void onIgniteLoad();

    public abstract P getPlugin();

    public abstract Logger getLogger();

    public void endSetup(String s) {
        getLogger().log(Level.SEVERE, s);
        if(!shutdown) {
            stop();
            shutdown = true;
        }
        throw new IllegalArgumentException("Disabling... " + s);
    }

    public abstract void stop();

    public void loadProperties() {
        if (!sqlpropertiesfile.exists()) {

            try {
                PropertiesFile.generateFresh(sqlpropertiesfile, new String[]{"hostname", "port", "username", "password", "database", "site-hostname","site-port", "site-username", "site-password","site-database","fileConnection-ip","fileConnection-port","fileConnection-type","fileConnection-user","fileConnection-password"}, new String[]{"localhost", "3306", "igniteuhc", "bfNPpvGUkQzFtxmT", "ignitenetwork", "igniteuhc.com", "3306", "site","NONE","thebubbl_site","localhost","21","FTP","root","NONE"});
            } catch (Exception e) {
                getLogger().log(Level.WARNING, "Could not generate fresh properties file");
            }
        }

        try {
            propertiesFile = new PropertiesFile(sqlpropertiesfile);
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Could not load SQL properties file", e);
            endSetup("Exception occurred when loading properties");
        }
    }

    public void initFileConnection() {
        //FTP connection type
        Class<? extends AbstractFileConnection> fileConnectionClass;
        switch (propertiesFile.getString("fileConnection-type")){
            case "FTP":
            case "TLS":
            case "SSL":
                fileConnectionClass = FTPFileConnection.class;
                break;
            case "SSH":
            case "SFTP":
                fileConnectionClass = SSHFileConnection.class;
                break;
            default:
                getLogger().log(Level.WARNING, "Invalid fileConnection-type");
                endSetup("Invalid fileconnection");
                return;
        }

        //FTP info
        try{
            fileConnection = AbstractFileConnection.create(fileConnectionClass, propertiesFile.getString("fileConnection-ip"), propertiesFile.getNumber("fileConnection-port").intValue());
        } catch (ParseException ex) {
            getLogger().log(Level.WARNING, "Could not load fileConnection information", ex);
            endSetup("Invalid fileConnection port");
        } catch (Exception ex) {
            getLogger().log(Level.WARNING, "Could not load fileConnection information", ex);
            endSetup("Invalid configuration");
        }
    }

    public void loadUpdatesTable() throws SQLException, ClassNotFoundException {

        if (!SQLUtil.tableExists(getConnection(), "updates")) {
            getLogger().log(Level.INFO, "Creating updates table");
            getConnection().executeSQL("CREATE TABLE `updates` (" +
                    "`artifact` VARCHAR(32) NOT NULL," +
                    "`version` INT(3) NOT NULL," +
                    "`url` VARCHAR(255) NOT NULL," +
                    "PRIMARY KEY (`artifact`)," +
                    "UNIQUE INDEX `UNIQUE KEY` (`artifact`)," +
                    "UNIQUE INDEX `UNIQUE URL` (`url`)," +
                    "INDEX `KEY` (`artifact`)" +
                    ")" +
                    ";");

            //log successful creation
            getLogger().log(Level.INFO, "Updates table created successfully!");
        }

    }

}
