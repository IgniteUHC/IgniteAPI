package com.igniteuhc.api.global.backend.resource;


import com.igniteuhc.api.global.plugin.IgniteCore;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.InputStream;

public class ResourceLoader {
    private final String internalfile;
    private YamlConfiguration configuration;

    public ResourceLoader(String internalfile) {
        this.internalfile = internalfile;
        InputStream stream = IgniteCore.getInstance().getResource(internalfile); //todo: Fix this
        try{
            configuration = YamlConfiguration.loadConfiguration(stream);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        load(configuration);
    }

    public void load(YamlConfiguration configuration) {

    }
}
