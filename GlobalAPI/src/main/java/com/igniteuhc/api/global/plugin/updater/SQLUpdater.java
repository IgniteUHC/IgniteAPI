package com.igniteuhc.api.global.plugin.updater;

import com.igniteuhc.api.global.sql.SQLConnection;

import java.sql.SQLException;

public interface SQLUpdater {
    void update(SQLConnection connection) throws SQLException, ClassNotFoundException;

    String getName();
}
